/*
- function that will count to a series of number depending on the input of the user.
- get the value of the input field. in order to get the value of the input, using a dot(.)
notation call out its value property.

5 WAYS to Select Elements (5 DOM Selectors)
- getElementByTagName() -- collective through the use of the element/tag name.
- getElementByClassName() -- collective through the use of their class attributes.
- getElementById() -- select a disctinct/specific through the use of its ID attribute.
- querySelector() -- neutral/versatile selection.
- querySelectorAll() -- collective approach when selecting multiple componenets/elements at once.
*/
function countUsingWhile() {
	let input1 = document.getElementById('task1').value;
	//solution to avoid negative number
	if (input1 <= 0) {
		//inform the user that input is NOT valid
		//innerHTML property
		let msg = document.getElementById('message');
		msg.innerHTML = 'Value Not Valid';
	} else {
	//what will happen if the condition is/not met.
	while (input1 !== 0){
		alert(input1);
		input1-- //decrease the value of input by 1 per iteration of the loop
		}
	}
}

/*
function that will count to a series of number depending on the value inserted by the user.
*/
function countUsingDoWhile() {
	//get the input of the user
	let number = document.getElementById('task2').value;
	// make sure the value is valid (will not accept 0 or less)
	if (number <= 0) {
		let displayText = document.getElementById('info')
		displayText.innerHTML = 'The number is not valid'
	} else {
		//PROCEED coz the value is VALID
		/*
		let number = Number(prompt('Give me a number'));
		do{
			console.log('Do While ' + number);
			number += 1;	
		}while (number <10)
		*/
		let indexStart = 1 //will count from 1 to n (depending on the value inserted by user)
		do {
			//block of code identified here will be executed first
			let displayText = document.getElementById('info');
			displayText.innerHTML = number + ' is Valid';
			alert('Count value: ' + indexStart);
			indexStart++
		} while (indexStart <= number);

	}
}


// FOR LOOP
// syntax: for (initialization; expression/condition; finalExpression/iteration)
// Task: count to a series number depending on the value inserted by user.
function countUsingForLoop() {
	let data = document.getElementById('task3').value;
	let res = document.getElementById('response');
	if (data <= 0) {
		res.innerHTML = 'Input Invalid';
	} else {
		//res.innerHTML = 'Input Valid';
		//initialization; condition; iteration/finalExp
		for (let startCount = 0; startCount <= data; startCount++) {
		//since the start of the count started with 0 then for every iteration we should add a value of 1 to eventually meet the condition and terminate the process.
		//describe what will happen per iteration
		alert('This is the value in this iteration: ' + startCount); //print each value of each iteration before the process terminates.
		}
	}
}

// [] How to access Elements of a string using repetition control structures.

function accessElementsInString() {
	let name = document.getElementById('userName').value;
	
	if (name !== '') {
		//response if truthy
		//Checker: alert('Value is Valid');
		textLength.innerHTML = 'The string is ' + name.length + ' characters long';
		//upon accessing elements inside a string, this can be done so using [] square brackets.
		//keep in mind we can access each element through the use of its index number/count.
		//the count will start from 0 (first character inside the string coreespons to the number 0), the next is 1 and up to the "nth" number
		/*
		console.log(name[0]);
		console.log(name[1]);
		console.log(name[2]);
		console.log(name[3]);
		console.log(name[4]);
		console.log(name[5]);
		console.log(name[6]);
		*/
	//will use concept of loops in order to produce a much more flexible response for the user's input
		//initialization ; condition ; iteration
	for (let startIndex = 0; startIndex < name.length; startIndex++) {
		//access each element and display it inside the console
		console.log(name[startIndex]);
	}

	} else {
		//response if falsy
		alert('Value is Invalid');
	}
}

/* RECAP
How to access elements of string? []
	- [value/index] 
	- index count always starts with ZERO(0)
	- 0 1 2 3 .. nth
*/

/*Detect if the word is a palindrome
	- Behavior: if the string provided is an odd number, the middle character does not need to be checked.
	- e.g. K A Y A K -> letter A will not be checked
	- we'll create a loop through half of the string's characters that checks
	- check if the leters at the front and at the back
*/
function detectPalindrome(){
	//1. get input using DOM selectors
	let word = document.getElementById('word').value;
	//to capture and let the message display from html
	let response = document.getElementById('detectPalindrome');
	//CHECKER -- alert(word);
	//2. data validation
	if (word !== ''){
		//identify how long the word is.
		let wrdLength = word.length;
		//initialization -> identify the starting point of the loop.
		//condition -> describes how the loop will progress and it terminates.
		//iteration -> how to advance to the next process/loop.
		for (let index = 0; index < wrdLength / 2; index++) {
			//instruction that will happen upon each iteration of the loop
			//we're trying to get the current element in the string according to the index count
			//get the last element of the string by -1
			if (word[index] !== word[wrdLength - 1 - index]){
			//reponse
			response.innerHTML = word + '<h3 class="text-primary"> is not a Palindrome</h3>';
			} else{
				console.log(word[index] + ' is the same as ' + word[wrdLength - 1 - index]);
			response.innerHTML = word + '<h3 class="text-success"> is a Palindrome</h3>';
			}
		}	
	}	else {
		//response - from JS to Html / display msg
		response.innerHTML = '<h3 class="text-danger">Value is Invalid</h3>';
	}
}

//create a function that will allow us to only display the odd numbers from a set of integers.
/* STRUCTURE
function getOddNumbers() {
	//target the value of the user's input
	let inputCount = document.getElementById('value4').value;
	//from html div>p
	let res = document.getElementById('getOddNum');
	//alert(inputCount);
	//validate the data to make sure we get positive numbers
	if (inputCount > 0) {
		//response if pass
		res.innerHTML = '<h3 class="text-warning">Proceed</h3>';
	} else {
		//reponse if fail
		res.innerHTML = '<h3 class="text-danger">The Number should be greater than 0</h3>'
	}
}
*/
function getOddNumbers() {
	//target the value of the user's input
	let inputCount = document.getElementById('value4').value;
	//from html div>p
	let res = document.getElementById('getOddNum');
	//alert(inputCount);
	//validate the data to make sure we get positive numbers
	if (inputCount > 0) {
		//response if pass
		//create a loop that will product the set of numbers depending on the input of the user.
		for (let count = 0; count <= inputCount; count++) {

		//lets create another logic that will tweak out the even numbers away from the set of numbers.
		if (count % 2 === 0) {
			//even number
			continue;
			}
			//print out the series of numbers inside the console.
			console.log(count);
		}
	} else {
		//reponse if fail
		res.innerHTML = '<h3 class="text-danger">The Number should be greater than 0</h3>'
	}
}

//modify the code above to return a series of numbers and display inside the console without the even numbers.